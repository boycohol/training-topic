﻿namespace Training_Topic.ExceptionHandler
{
    public class ExistedException : Exception
    {
        public ExistedException(string message) : base(message)
        {
        }
    }
}
