﻿using Training_Topic.ExceptionHandler.configException;

namespace Training_Topic.ExceptionHandler
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = (int)ExceptionStatusCode.GetExceptionStatusCode(ex);
                context.Response.ContentType= "text/plain";
                await context.Response.WriteAsync(ex.Message);
            }
        }
    }

    public static class MiddlewareExtension
    {
        public static IApplicationBuilder UseExceptionMiddleware(
        this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
