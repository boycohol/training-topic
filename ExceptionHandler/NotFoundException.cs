﻿using System.Net;

namespace Training_Topic.ExceptionHandler
{
    public class NotFoundException : Exception
    {
        public HttpStatusCode code { get; set; }
        public NotFoundException(string message) : base(message) { }
        public NotFoundException(HttpStatusCode code, string message) : base(message)
        {
            this.code = code;
        }
    }
}
