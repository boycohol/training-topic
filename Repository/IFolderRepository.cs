﻿using Microsoft.AspNetCore.Identity;
using Training_Topic.Entity;
using Training_Topic.Model;

namespace Training_Topic.Repository
{
    public interface IFolderRepository
    {
        public Task<List<FolderModel>> getAllFoldersAsync();
        public Task<int> createFolderAsync(FolderModel model);
        public Task<FolderModel> getFolderByIdAsync(int id);
        public Task<string> deleteFolderByIdAsync(int id);
        public Task<int> updateFolderAsync(FolderModel model);
    }
}
