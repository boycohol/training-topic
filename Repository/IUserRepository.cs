﻿using Microsoft.AspNetCore.Identity;
using Training_Topic.Entity;
using Training_Topic.Model;

namespace Training_Topic.Repository
{
    public interface IUserRepository
    {
        public Task<IdentityResult> SignUpAsync(SignUpModel signUpModel);
        public Task<string> SignInAsync(SignInModel signInModel);
    }
}
