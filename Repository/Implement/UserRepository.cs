﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Training_Topic.Data;
using Training_Topic.Entity;
using Training_Topic.Model;

namespace Training_Topic.Repository.Implement
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IConfiguration configuration;
        private readonly ApplicationDBContext context;

        public UserRepository(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationDBContext context, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
            this.context = context;
            

        }
        public async Task<string> SignInAsync(SignInModel signInModel)
        {
            var authentication = await signInManager.PasswordSignInAsync(signInModel.Email, signInModel.Password, false, false);
            if (!authentication.Succeeded)
            {
                return "Email or Password is wrong!";
            }
            var authClaims = new List<Claim> {
                new Claim(ClaimTypes.Email, signInModel!.Email),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
            };
            var authKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:SecretKey"]!));

            var token = new JwtSecurityToken(
                issuer: configuration["Jwt:ValidIssuer"],
                audience: configuration["Jwt:ValidAudience"],
                expires: DateTime.Now.AddHours(1),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authKey, SecurityAlgorithms.HmacSha256Signature)
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<IdentityResult> SignUpAsync(SignUpModel signUpModel)
        {
            var user = new User()
            {
                Email = signUpModel.Email,
                FirstName = signUpModel.FirstName,
                LastName = signUpModel.LastName,
                UserName = signUpModel.Email
            };
            return await userManager.CreateAsync(user, signUpModel.Password);
        }
    }
}
