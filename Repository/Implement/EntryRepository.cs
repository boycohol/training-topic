﻿using Microsoft.EntityFrameworkCore;
using Training_Topic.Data;
using Training_Topic.Entity;
using Training_Topic.ExceptionHandler;
using Training_Topic.Helper;
using Training_Topic.Model;

namespace Training_Topic.Repository.Implement
{
    public class EntryRepository : IEntryRepository
    {
        private readonly ApplicationDBContext context;
        private readonly ApplicationMapper mapper;

        public EntryRepository(ApplicationDBContext context, ApplicationMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
        public async Task<object> createEntryAsync(List<EntryModel> entry)
        {
            var responseValue = new Dictionary<string, int>();
            foreach (var e in entry)
            {
                var folder = await context.Folders.SingleOrDefaultAsync(f => f.Id == e.folderId);
                var newEntry = new Entry()
                {
                    Folder = folder!,
                    FolderForeignKey = folder!.Id,
                    question = e.question,
                    response = e.response
                };
                context.Entries.Add(newEntry);
                await context.SaveChangesAsync();
                responseValue.Add("Id: ", newEntry.Id);
            }

            return responseValue;
        }

        public async Task<string> deleteEntryByIdAsync(int id)
        {
            var entry = await context.Entries.SingleOrDefaultAsync(e => e.Id == id);
            if (entry == null)
            {
                throw new NotFoundException("Not found entry " + id);
            }
            context.Entries.Remove(entry);
            await context.SaveChangesAsync();
            return "Delete success!";
        }

        public async Task<List<EntryModel>> getAllEntriesAsync()
        {
            var entries = await context.Entries.Select(e => new EntryModel
            {
                Id = e.Id,
                question = e.question,
                response = e.response,
                folderId = e.Folder.Id,
            }).ToListAsync();
            return entries;
        }

        public async Task<List<EntryModel>> getAllEntriesBySearchAsync(string search)
        {
            return await context.Entries.Where(e =>
                e.question.Contains(search) ||
                e.response.Contains(search)
            ).Select(e => new EntryModel
            {
                Id = e.Id,
                question = e.question,
                response = e.response,
                folderId = e.Folder.Id,
            }).ToListAsync();
        }

        public async Task<List<EntryModel>> getAllEntriesInFolderAsync(int id)
        {
            return await context.Entries.Where(e=>e.Folder.Id==id).Select(e => new EntryModel
            {
                Id = e.Id,
                question = e.question,
                response = e.response,
                folderId = e.Folder.Id,
            }).ToListAsync() ;
        }

        public async Task<EntryModel> getEntryByIdAsync(int id)
        {
            var entry = await context.Entries.SingleOrDefaultAsync(e => e.Id == id);
            if (entry == null)
            {
                throw new NotFoundException($"Entry has id = {id} not found!");
            }
            return mapper.mapEntry(entry);
        }

        public async Task<string> UpdateEntryByIdAsync(EntryModel model)
        {
            var entry = await context.Entries.SingleOrDefaultAsync(e => e.Id == model.Id);
            if (entry == null)
            {
                throw new NotFoundException("not found entry " + model.Id);
            }
            entry.question = model.question;
            entry.response = model.response;
            entry.FolderForeignKey = model.folderId;
            await context.SaveChangesAsync();
            return "Update success!";
        }
    }
}
