﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;
using Training_Topic.Data;
using Training_Topic.Entity;
using Training_Topic.ExceptionHandler;
using Training_Topic.Helper;
using Training_Topic.Model;

namespace Training_Topic.Repository.Implement
{
    public class FolderRepository : IFolderRepository
    {
        private readonly ApplicationDBContext context;
        private readonly ApplicationMapper mapper;

        public FolderRepository(ApplicationDBContext context, ApplicationMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
        public async Task<int> createFolderAsync(FolderModel model)
        {
            var name = model.Name;
            var parentFolderName = model.ParentFolder;
            var newFolder = new Folder()
            {
                Name = name,
                ParentFolder = parentFolderName,
            };
            context.Folders.Add(newFolder);
            await context.SaveChangesAsync();

            return newFolder.Id;
        }

        public async Task<string> deleteFolderByIdAsync(int id)
        {
            var folder = await context.Folders.SingleOrDefaultAsync(f => f.Id == id);
            if (folder == null)
            {
                throw new NotFoundException($"Not found folder with id: {id}");
            }
            context.Remove(folder);
            await context.SaveChangesAsync();
            return "Delete Success";
        }

        public async Task<List<FolderModel>> getAllFoldersAsync()
        {
            return await context.Folders.Select(f => new FolderModel
            {
                Id = f.Id,
                Name = f.Name,
                ParentFolder = f.ParentFolder,
            }).ToListAsync();
        }
        public async Task<FolderModel> getFolderByIdAsync(int id)
        {
            var folder = await context.Folders.SingleOrDefaultAsync(f => f.Id == id);
            if (folder == null)
            {
                throw new NotFoundException("Can't not found folder id: " + id);
            }
            return mapper.mapFolder(folder);
        }

        public async Task<int> updateFolderAsync(FolderModel model)
        {
            var folder = await context.Folders.SingleOrDefaultAsync(f => f.Id == model.Id);
            if (folder == null)
            {
                throw new NotFoundException("Can't not found folder id :" + model.Id);
            }
            folder.ParentFolder = model.ParentFolder;
            folder.Name = model.Name;
            await context.SaveChangesAsync();
            return folder.Id;
        }
    }
}
