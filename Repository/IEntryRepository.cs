﻿using Training_Topic.Entity;
using Training_Topic.Model;

namespace Training_Topic.Repository
{
    public interface IEntryRepository
    {
        public Task<List<EntryModel>> getAllEntriesAsync();
        public Task<EntryModel> getEntryByIdAsync(int id);
        public Task<object> createEntryAsync(List<EntryModel> entry);
        public Task<string> deleteEntryByIdAsync(int id);
        public Task<string> UpdateEntryByIdAsync(EntryModel model);
        public Task<List<EntryModel>> getAllEntriesBySearchAsync(string search);
        public Task<List<EntryModel>> getAllEntriesInFolderAsync(int id);
    }
}
