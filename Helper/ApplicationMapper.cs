﻿using AutoMapper;
using Training_Topic.Entity;
using Training_Topic.Model;

namespace Training_Topic.Helper
{
    public class ApplicationMapper
    {
        public FolderModel mapFolder(Folder folder)
        {
            var model = new FolderModel();
            model.Id= folder.Id;
            model.Name = folder.Name;
            model.ParentFolder = folder.ParentFolder;
            return model;
        }
        public EntryModel mapEntry(Entry entry)
        {
            return new EntryModel()
            {
                Id= entry.Id,
                folderId = entry.Folder.Id,
                question = entry.question,
                response= entry.response,
            };
        }
    }
}
