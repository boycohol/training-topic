﻿using Microsoft.EntityFrameworkCore;

namespace Training_Topic.Helper
{
    public class Paging<T>
    {
        public async Task<List<T>> createPaging(IQueryable<T> query, int pageIndex, int pageSize)
        {
            var items = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
            return items;
        }

    }
}
