﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Training_Topic.Entity;
using Training_Topic.Model;
using Training_Topic.Repository;
using Training_Topic.Response;

namespace Training_Topic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FoldersController : ControllerBase
    {
        private readonly IFolderRepository folderRepository;

        public FoldersController(IFolderRepository folderRepository)
        {
            this.folderRepository = folderRepository;
        }
        [HttpGet]
        public async Task<ResponseModel> getAllFolders()
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await folderRepository.getAllFoldersAsync());
        }
        [HttpGet("{id}")]
        public async Task<ResponseModel> getFolderById(int id)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await folderRepository.getFolderByIdAsync(id));
        }
        [HttpPost]
        public async Task<ResponseModel> createFolder([FromBody] FolderModel model)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await folderRepository.createFolderAsync(model));

        }
        [HttpPut]
        public async Task<ResponseModel> updateFolder([FromBody] FolderModel model)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await folderRepository.updateFolderAsync(model));
        }
        [HttpDelete("{id}")]
        public async Task<ResponseModel> deleteFolder(int id)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await folderRepository.deleteFolderByIdAsync(id));
        }
    }
}
