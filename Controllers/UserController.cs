﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Training_Topic.Model;
using Training_Topic.Repository;
using Training_Topic.Response;

namespace Training_Topic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository userRepository;

        public UserController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        [HttpPost("login")]
        public async Task<ResponseModel> loginUser(SignInModel model)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await userRepository.SignInAsync(model));
        }
        [HttpPost("register")]
        public async Task<ResponseModel> registerUser(SignUpModel model)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await userRepository.SignUpAsync(model));
        }
    }
}
