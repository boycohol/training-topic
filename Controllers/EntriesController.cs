﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Training_Topic.Model;
using Training_Topic.Repository;
using Training_Topic.Response;

namespace Training_Topic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EntriesController : ControllerBase
    {
        private readonly IEntryRepository entryRepository;

        public EntriesController(IEntryRepository entryRepository)
        {
            this.entryRepository = entryRepository;
        }
        [HttpGet]
        public async Task<ResponseModel> getAllEntries()
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await entryRepository.getAllEntriesAsync());
        }
        [HttpGet("{id}")]
        public async Task<ResponseModel> getEntryById(int id)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await entryRepository.getEntryByIdAsync(id));
        }
        [HttpGet("search")]
        public async Task<ResponseModel> searchEntries(string search)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await entryRepository.getAllEntriesBySearchAsync(search));
        }
        [HttpGet("folder/{id}")]
        public async Task<ResponseModel> getEntriesInFolder(int id)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK,"Success!",await entryRepository.getAllEntriesInFolderAsync(id));
        }
        [HttpPost]
        public async Task<ResponseModel> createEntry([FromBody] List<EntryModel> models)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await entryRepository.createEntryAsync(models));
        }
        [HttpDelete("{id}")]
        public async Task<ResponseModel> deleteEntry(int id)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await entryRepository.deleteEntryByIdAsync(id));

        }
        [HttpPut]
        public async Task<ResponseModel> updateEntry([FromBody] EntryModel model)
        {
            return new ResponseModel(System.Net.HttpStatusCode.OK, "Success!", await entryRepository.UpdateEntryByIdAsync(model));

        }
    }
}
