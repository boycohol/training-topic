﻿using System.ComponentModel.DataAnnotations;

namespace Training_Topic.Model
{
    public class SignInModel
    {
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
