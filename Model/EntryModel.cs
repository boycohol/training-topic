﻿using System.Security.Principal;

namespace Training_Topic.Model
{
    public class EntryModel
    {
        public int Id { get; set; }
        public string question { get; set; }
        public string response { get; set; }
        public int folderId { get; set; }
    }
}
