﻿using System.ComponentModel.DataAnnotations;

namespace Training_Topic.Model
{
    public class SignUpModel
    {
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

    }
}
