﻿using System.ComponentModel.DataAnnotations;

namespace Training_Topic.Model
{
    public class FolderModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string ParentFolder { get; set; }
    }
}
