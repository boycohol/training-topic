﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Training_Topic.Entity
{
    public class Entry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string question { get; set; }
        public string response { get; set; }


        //Relationship
        public int FolderForeignKey { get; set; }
        public virtual Folder Folder { get; set; }
    }
}
