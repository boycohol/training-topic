﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Training_Topic.Entity
{
    public class Folder
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string  Name { get; set; }
        public string ParentFolder { get; set; }
        public virtual ICollection<Entry>? Entries { get; set;}
    }
}
