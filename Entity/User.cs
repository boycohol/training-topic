﻿using Microsoft.AspNetCore.Identity;

namespace Training_Topic.Entity
{
    public class User : IdentityUser
    { 
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
    }
}
