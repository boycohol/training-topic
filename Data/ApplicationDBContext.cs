﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Training_Topic.Entity;

namespace Training_Topic.Data
{
    public class ApplicationDBContext : IdentityDbContext<User>
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
        }
        public DbSet<Folder> Folders { get; set; }
        public DbSet<Entry> Entries { get; set; }
        /*public DbSet<User> Users { get; set; }*/

        //Set Relationship
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Entry>(e =>
            {
                e.HasOne(e => e.Folder).WithMany(e => e.Entries)
                .HasForeignKey(e => e.FolderForeignKey).HasConstraintName("FK_Entry_Folder");
            });
        }
    }
}
